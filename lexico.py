arq = open('saidaAFD', 'r')
AFD = arq.readlines()
entrada = open('codigo.txt', 'r')
code = []
code = entrada.readlines()
alfabeto = []
separadores = ['<', '>', '=', ',', '.', '|', '~', '+', '-', '*', '/', '!', ' ', '\n']
afd = {}

estados = int(AFD[0])
aux = AFD[1].split()
finais = AFD[estados+2].split()

#criação do alfabeto
for a in range(0,len(aux)):
	alfabeto.append(aux[a])

#criação do afnd (dicionario)
for a in range(0,len(alfabeto)):
	afd[alfabeto[a]] = []

for i in range(2, estados+2):
    slice = AFD[i].split()
    for j in range(0, len(alfabeto)):
        afd[alfabeto[j]].append(slice[j])

for i in range(0, len(code)):
    words = []
    ini = 0
    fim = 0
    for j in range(0, len(code[i])):
        if(code[i][j] not in separadores):
            fim += 1
        else:
            words.append(code[i][ini:fim])
            if(code[i][j] != ' ' and code[i][j] != '\n'):
                words.append(code[i][fim])
            ini = fim = fim+1
words.remove('')
print(words)
